import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saludo',
  templateUrl: './saludo.component.html',
  styleUrls: ['./saludo.component.css']
})
export class SaludoComponent implements OnInit {
  
  nombre = "Angular";
  mensaje = "Este mensaje por default";

  constructor() { }

  ngOnInit() {
  }
 
  submitted = false;
 
  onSubmit() { this.submitted = true; }
 
  newHero() {
   
  }

}
