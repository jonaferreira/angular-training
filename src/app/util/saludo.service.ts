import { Injectable } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms'

@Injectable({
  providedIn: 'root'
})
export class SaludoService {

  constructor() { }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    fullName: new FormControl('', Validators.required)
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      fullName: ''
    });
  }
}
